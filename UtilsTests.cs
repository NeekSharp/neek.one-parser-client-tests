﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ParserClient.Test
{
    [TestFixture]
    class UtilsTests
    {
        [Test]
        public void DeserializeJson()
        {
            string json = "{\"data\":[\"apple\",\"orange\"]}";


            HashSet<string> result = Utils.JsonConverter.KnownPagesToHashSet(json);

            Assert.IsTrue(result.Count() == 2);
        }

        [Test]
        public void SerializeJson()
        {
            HashSet<string> set = new HashSet<string>()
            {
                "a",
                "b"
            };

            string result = Utils.JsonConverter.KnownPagesToJson(set);

            Assert.IsTrue(result.Equals("{\"data\":[\"a\",\"b\"]}"));
        }
    }
}
