﻿using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using ParserClient;
using ParserClient.Utils;
using System.Threading.Tasks;
using ParserClient.Crawler;
using CommonLibrary;

namespace ParserClient.Test
{
    [TestFixture]
    public class PageRequesterTests
    {
        [Test]
        public void DownloadPage_CorrectLink_HttpStatusOkAndHtmlNotNull()
        {
            // Arrange
            PageRequester requester = new PageRequester("*");

            // Act
            Task<WebPage> task = requester.DowloadPageAsync(new Link("http://google.ru"));
            WebPage page = task.Result;

            // Assert
            Assert.IsTrue(page.HttpStatus == System.Net.HttpStatusCode.OK);
            Assert.IsTrue(page.Html != null);
        }

        [Test]
        public void DownloadPage_NotCorrectLink_HttpStatusNotFoundAndHtmlEqNull()
        {
            // Arrange
            PageRequester requester = new PageRequester("*");

            // Act
            Task<WebPage> task = requester.DowloadPageAsync(new Link("http://habrahabr.ru/sadwcdcdssdcsdc"));
            WebPage page = task.Result;

            // Assert
            Assert.IsTrue(page.HttpStatus == System.Net.HttpStatusCode.NotFound);
            Assert.IsTrue(page.Html == null);
        }

        [Test]
        public void RobotsTxtTest_FileWithDelay_CorrectDelayDetecting()
        {
            PageRequester req = new PageRequester("*");
            WebPage page = req.DowloadPage(new Link("http://habrahabr.ru/robots.txt"));
            //string str = .DocumentNode.InnerHtml;
            RobotsTxt.Robots robot = RobotsTxt.Robots.Load(page.Html);

            int delay = (int)robot.CrawlDelay("UserAgent");

            Assert.IsTrue(delay > 1000);
        }

        [Test]
        public void RobotsTxtTest_AllowedAndDisallowedLinks_CorrectDetectDesallowedPaths()
        {
            PageRequester req = new PageRequester("*");
            WebPage page = req.DowloadPage(new Link("http://andercomp.ru/robots.txt"));
            //string str = page.Html.DocumentNode.InnerHtml;
            RobotsTxt.Robots robot = RobotsTxt.Robots.Load(page.Html);

            Assert.IsTrue(robot.IsPathAllowed("*","/"));
            Assert.IsFalse(robot.IsPathAllowed("*", "/cgi-bin/"));
            Assert.IsTrue(robot.IsPathAllowed("*", "/category/obzor-igr"));
            Assert.IsTrue(robot.IsPathAllowed("*", "/category/e-to-interesno"));
        }

        [Test]
        public void RobotsTxtTest_ReactionOnEmptyFile_AllPathAllowed()
        {
            RobotsTxt.Robots robot = RobotsTxt.Robots.Load("");

            Assert.IsTrue(robot.IsPathAllowed("*", "/"));
            Assert.IsTrue(robot.IsPathAllowed("*", "/cgi-bin/"));
            Assert.IsTrue(robot.IsPathAllowed("*", "/category/obzor-igr"));
            Assert.IsTrue(robot.IsPathAllowed("*", "/category/e-to-interesno"));
        }
    }
}
