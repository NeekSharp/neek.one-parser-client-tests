﻿using System;
using NUnit.Framework;
using CommonLibrary;
using System.Linq;
using System.Threading.Tasks;

namespace ParserClient.Test
{
    [TestFixture]
    public class ImageProcessor
    {
        // Плохой тест
        [Test]
        public async Task FindLagestImageByHeaders()
        {
            ParserClient.Utils.PageRequester requester = new Utils.PageRequester("neek.one");
            WebPage page = requester.DowloadPage(new CommonLibrary.Link("http://habrahabr.ru"));
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(page.Html);
            Content content = new Content();
            content.Link = new CommonLibrary.Link("http://habrahabr.ru/");

            Uri uri = await ParserClient.Images.ImageProcessor.FindImageByHeaders(content,doc.DocumentNode.Descendants("body").First(),200,200);

            Assert.IsNotNull(uri);
        }
    }
}
