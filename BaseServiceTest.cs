﻿using System;
using NUnit.Framework;
using ParserClient.Services;
using System.Threading.Tasks;
using System.Threading;

namespace ParserClient.Test
{
    [TestFixture]
    class BaseServiceTest
    {
        [Test]
        public void RunTask_TaskWithException_ExceptionCached()
        {
            FindNewArticles service = new FindNewArticles(null, null, ConsoleColor.Black);
            

            CancellationTokenSource cts = new CancellationTokenSource();
            Assert.DoesNotThrow(() =>
            {
                service.RunTask(new Task(() =>
                {
                    Task.Delay(500);
                    throw new NullReferenceException();
                }), cts);
            });
        }
    }
}
